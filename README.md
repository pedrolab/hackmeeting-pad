See history of pad as git commits

# hackmeeting-pad

cron script in `/etc/cron.daily/hackmeeting-pad`

```
#!/bin/bash

cd /root/hackmeeting-pad
/usr/bin/wget 'https://scalar.vector.im/etherpad/p/!BGYpgcXIcBuEsNJmcj_matrix.org_HM/export/txt' -O hackmeeting.pad.txt
/usr/bin/git add hackmeeting.pad.txt
/usr/bin/git commit -m "daily cron $(date +'%Y-%m-%d_%H:%M:%S')"
/usr/bin/git push
```
